const restaurantModel = require('../Model/restaurantmodel');
const userModel = require('../Model/usermodel');

async function updateUserById(req, res) {
    try {
        let id = req.params.id;
        let updateObj = req.body;
        // console.log(req.body)
        let user = await userModel.findById(id);

        for (key in updateObj) {
            user[key] = updateObj[key];
        }

        let updatedUser = await user.save();
        res.status(201).json({
            message: "Updated User",
            data: updatedUser
        })

    }
    catch (error) {
        res.status(501).json({
            message: "Failed to update user",
            error
        })
    }
}

async function getAllActiveRestaurant(req, res){
    try{
        let allRestaurant = await restaurantModel.find({isActive:true});
        let length = allRestaurant.length;
        if(length)
            res.json({
                "message":"fetched all restaurant successfully",
                allRestaurant
            })
        else
            res.json({
                "message":"no restaurant found"
            })
    }
    catch(err){
        res.json({
            "message":"error occured while fetching all restaurant",
            err
        })
    }
}

async function getResultBasedOnCity(req, res){
    try{
        let city = req.body.city;
        // console.log(filter);
        let allRestaurantInCity = await restaurantModel.find({city:city, isActive:true})
        res.json({
            allRestaurantInCity
        })
    }
    catch(err){
        res.json({
            "message":"error occured while filtering all restaurant",
            err
        })
    }
}

async function getResultBasedOnName(req, res){
    try{
        let {name} = req.body;
        console.log(name);
        let restaurant = await restaurantModel.find({name:name, isActive:true})
        res.json({
            restaurant
        })
    }
    catch(err){
        res.json({
            "message":"error occured while fetching restaurant",
            err
        })
    }
}

async function getResultBasedOnPriceRange(req, res){
    try{
        let {low, high} = req.body;
        // console.log(req)
        // console.log(low, high);
        let restaurant = await restaurantModel.find({isActive:true, averageDinningPrice: {$gte:low, $lte : high}})
        res.json({
            restaurant
        })
    }
    catch(err){
        res.json({
            "message":"error occured while fetching price range",
            err
        })
    }
}

async function deleteUserById(req, res){
    try {
        let id = req.params.id;
        let userFromDb = await userModel.findByIdAndDelete(id)

        if (userFromDb === null) {
            res.json({
                "message": "user not exists"
            })
        }
        else {
            res.json({
                "message": "user deleted successfully!!",
                userFromDb
            })
        }
    }
    catch (err) {
        res.json({
            "message": "error occured in deleting user",
            err
        })
    }
}

module.exports = {
    updateUserById:updateUserById,
    getAllActiveRestaurant:getAllActiveRestaurant,
    getResultBasedOnCity:getResultBasedOnCity,
    getResultBasedOnName:getResultBasedOnName,
    getResultBasedOnPriceRange:getResultBasedOnPriceRange,
    deleteUserById:deleteUserById
}