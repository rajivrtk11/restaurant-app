const restaurantModel = require('../Model/restaurantmodel');
const userModel = require('../Model/usermodel');

async function addRestaurant(req, res) {
    try {
        let restData = req.body;
        // console.log(restData);
        let newRest = await restaurantModel.create({
            owner: restData.owner,
            name: restData.name,
            city: restData.city,
            averageDinningPrice: restData.averageDinningPrice,
            isActive: restData.isActive
        })

        let ownerId = req.body.uid;
        let owner = await userModel.find({ _id: ownerId });
        owner[0].restId.push(newRest.id);
        await owner[0].save();

        res.status(201).json({
            message: "Restaurant created successfully",
            newRest
        })
    }
    catch (err) {
        res.json({
            message: "Restaurant not created",
            error: err
        })
    }
}

async function editRestaurantById(req, res) {
    try {
        let id = req.params.id;
        let updateRest = req.body;
        // console.log(updateRest);
        let restFromDb = await restaurantModel.findById(id);
        for (key in updateRest) {
            restFromDb[key] = updateRest[key];
        }

        // console.log(restFromDb);
        let updatedRestaurant = await restFromDb.save();
        res.json({
            updatedRestaurant
        })
    }
    catch (err) {
        res.json({
            "message": "error occured in updating data",
            err
        })
    }
}

async function deleteRestaurantById(req, res) {
    try {
        let id = req.params.id;
        let { uid } = req.body;
        let restFromDb = await restaurantModel.findByIdAndDelete(id)

        if (restFromDb === null) {
            res.json({
                "message": "nothing to be deleted"
            })
        }
        else {
            let owner = await userModel.findById(uid);
            let delIdx = owner.restId.filter((ele, idx) => idx == id ? idx : -1);
            if (delIdx[0] != -1) {
                // swap first and last index and delete from array
                let length = owner.length;
                let temp = owner[delIdx[0]];
                owner[delIdx[0]] = owner[length - 1];
                owner[length - 1] = temp;
                owner.restId.$pop();
                await owner.save();
            }


            res.json({
                "message": "deleted data successfully!!",
                restFromDb
            })
        }
    }
    catch (err) {
        res.json({
            "message": "error occured in deleting data",
            err
        })
    }
}

async function getAllRestaurantOfOwner(req, res) {
    try {
        let owner = req.body.owner;
        let allRestaurantOfOwner = await restaurantModel.find({ owner: owner })
        res.json({
            allRestaurantOfOwner
        })
    }
    catch (err) {
        res.json({
            "message": "error occured in deleting data",
            err
        })
    }
}

async function getAllInactiveRestaurantOfOwner(req, res) {
    try {
        let owner = req.body.owner;
        let allRestaurantOfOwner = await restaurantModel.find({ owner: owner, isActive: false })
        res.json({
            allRestaurantOfOwner
        })
    }
    catch (err) {
        res.json({
            "message": "error occured in deleting data",
            err
        })
    }
}

module.exports = {
    addRestaurant: addRestaurant,
    editRestaurantById: editRestaurantById,
    deleteRestaurantById: deleteRestaurantById,
    getAllRestaurantOfOwner: getAllRestaurantOfOwner,
    getAllInactiveRestaurantOfOwner: getAllInactiveRestaurantOfOwner,
}