const mongoose = require('mongoose');
const {DB_LINK} = require('../config/secrets');

mongoose.connect(DB_LINK, { useNewUrlParser: true, useUnifiedTopology: true }).then((db)=>{
    console.log("connected to db");
})

let restaurantSchema = new mongoose.Schema({
    owner:{
        type:String, 
        required:true
    },
    name:{
        type:String, 
        required:true,
        unique:true
    },
    city:{
        type:String, 
        required:true
    },
    averageDinningPrice:{
        type:Number, 
        required:true
    },
    isActive:{
        type:Boolean,
        required:true
    }
    
})

const restaurantModel = new mongoose.model("restaurantcollections", restaurantSchema);
module.exports = restaurantModel;