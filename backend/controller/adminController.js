const restaurantModel = require("../Model/restaurantmodel");
const userModel = require("../Model/usermodel");

async function getAllRestaurants(req, res) {
    try {
        let allRestaurants = await restaurantModel.find();
        let length = allRestaurants.length;
        if (length)
            res.json({
                "message": "all restaurants fetched successfully",
                allRestaurants
            })
        else
            res.json({
                "message": "no restaurant found",
                allRestaurants
            })
    }
    catch (err) {
        res.json({
            "message": "error occured while fetching restaurants",
            allRestaurants
        })
    }
}

async function changeRoleById(req, res) {
    try {
        let { id, updatedRole } = req.body;
        let user = await userModel.findById(id);

        user.role = updatedRole;
        let newRole = await user.save();
        res.json({
            "message": "role updated successfully",
            newRole
        })

    }
    catch (err) {
        res.json({
            "message": "error occured while fetching data",
            err
        })
    }
}

async function changeOwnerToUser(req, res) {
    try {
        let { ownerId, adminId } = req.body;

        let ownerInfo = await userModel.findById({ _id: ownerId });
        let adminInfo = await userModel.findById({ _id: adminId });
        let allRestaurants = await restaurantModel.find({ owner: ownerInfo.name });
        // loop all owner restro and change name to admin
        allRestaurants.forEach(async (element) => {
            element.owner = adminInfo.name;
            await element.save();
        })
        // change role of owner to user
        ownerInfo.role = "user";
        await ownerInfo.save();

        // add restro to admin restro array
        for (let i = 0; i < allRestaurants.length; i++) {
            let id = allRestaurants[i].id;
            adminInfo.restId.push(id);
            await adminInfo.save();

        }
        res.json({
            "message": "owner to user changed successfully",
            allRestaurants
        })
    }
    catch (err) {
        res.json({
            "message": "error occured while changing the owner to user",
            err
        })
    }
}

async function changeUserToOwner(req, res){
    try {
        let { userId, adminId } = req.body;
        let userInfo = await userModel.findById({ _id: userId });
        let adminInfo = await userModel.findById({ _id: adminId });
        
        // intersection of admin and user restro
        const filteredRestroArray = userInfo.restId.filter(value => adminInfo.restId.includes(value));
        // select only admin restro
        const filteredAdminArray = adminInfo.restId.filter(value => !userInfo.restId.includes(value)); 
        // console.log(filteredRestroArray);

        // loop all filtered  restro and change name to user
        filteredRestroArray.forEach(async (id) => {
            let restro = await restaurantModel.findById({_id:id});
            restro.owner = userInfo.name;
            await restro.save();
        })
        // change role of user to owner
        let userToOwner = await userModel.findOneAndUpdate({ _id: userId }, { restId: filteredRestroArray, role:"owner" }, {
            new: true
        })

        await userModel.findOneAndUpdate({ _id: adminId }, { restId: filteredAdminArray }, {
            new: true
        })

        res.json({
            "message": "This user changed to owner successfully",
            userToOwner
        })
    }
    catch (err) {
        res.json({
            "message": "error occured while changing the owner to user",
            err
        })
    }
}

module.exports = {
    getAllRestaurants: getAllRestaurants,
    changeRoleById: changeRoleById,
    changeOwnerToUser: changeOwnerToUser,
    changeUserToOwner:changeUserToOwner
}