const express = require('express');
const { isLoggedIn, login, signup } = require('../controller/authController');
const userRouter = express.Router();
const { updateUserById, getResultBasedOnPriceRange,
         getResultBasedOnName, getResultBasedOnCity, getAllActiveRestaurant } = require("../controller/userController");


userRouter.post('/signup', signup);
userRouter.post('/login', login);
userRouter.use("",isLoggedIn);
userRouter.patch('/updateById/:id', updateUserById);
userRouter.get("/filterOnPriceRange", getResultBasedOnPriceRange)
userRouter.get("/filterOnName", getResultBasedOnName);
userRouter.get("/filterOnCity", getResultBasedOnCity);
userRouter.get("/allActiveRestaurant", getAllActiveRestaurant);

module.exports = userRouter;
