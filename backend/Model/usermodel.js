const mongoose = require("mongoose");
const { DB_LINK } = require("../config/secrets");

mongoose.connect(
    DB_LINK,
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then((db) => {
    console.log("Connected to db !!!");
  });

let userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    minlength: [6, "Password must be greater than 6 characters"],
    required: true
  },
  confirmPassword: {
    type: String,
    minlength: [6, "Password must be greater than 6 characters"],
    validate: {
      validator: function () {
        return this.password == this.confirmPassword;
      },
      message: "Password didn't matched !!"
    }
  },
  role: {
    type: String,
    enum: ["admin", "user", "owner", ],
    default: "user"
  },
  pImage: {
    type: String,
    default: "/images/users/default.png"
  },
  restId : {
    type:[String]
  }
})

const userModel = mongoose.model("userscollection", userSchema);
module.exports = userModel;