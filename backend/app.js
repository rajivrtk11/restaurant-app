const express = require('express');
const app = express();
const userRouter = require('./router/userRouter');
const ownerRouter = require('./router/ownerRouter');
const adminRouter = require('./router/adminRouter');
app.use(express.json())
require('dotenv').config();

app.use('/api/users', userRouter)
//api's for restaurant for restaurant
app.use('/api/restaurant', ownerRouter);

// api's for admin
app.use('/api/admin', adminRouter)

app.use("", (req, res) => {
    res.status(404).json({
        "message": "page not found"
    });
})

const PORT = process.env.PORT;
app.listen(PORT, () => {
    console.log(`app is listening at port http://localhost:${PORT}`)
})