const express = require('express');
const { getAllRestaurants, changeRoleById, changeOwnerToUser, changeUserToOwner } = require('../controller/adminController');
const { signup, isAuthentic, isLoggedIn } = require('../controller/authController');
const adminRouter = express.Router();
const { addRestaurant, editRestaurantById, deleteRestaurantById } = require('../controller/ownerController');
const { updateUserById, deleteUserById } = require('../controller/userController');

adminRouter.post('/createUser', signup);

// adminRouter.use("", isLoggedIn, isAuthentic)
adminRouter.post('/updateUser', updateUserById);
adminRouter.post('/deleteUser', deleteUserById);

adminRouter.get('/allRestaurants', getAllRestaurants)
adminRouter.post('/addRestaurant', addRestaurant);
adminRouter.post('/editRestaurant', editRestaurantById);
adminRouter.post('/deleteRestaurant', deleteRestaurantById);

// adminRouter.use("", ); 
adminRouter.get('/changeRole', changeRoleById);
adminRouter.post('/changeOwnerToUser', changeOwnerToUser);
adminRouter.post('/changeUserToOwner', changeUserToOwner);

module.exports = adminRouter