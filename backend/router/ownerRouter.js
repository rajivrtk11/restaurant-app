const express = require('express');
const { isLoggedIn, ownerAuthentication } = require('../controller/authController');
const { addRestaurant, editRestaurantById, deleteRestaurantById, getAllRestaurantOfOwner, getAllInactiveRestaurantOfOwner } = require('../controller/ownerController');
const ownerRouter = express.Router();

// ownerRouter.use("", isLoggedIn, ownerAuthentication);
ownerRouter.post('/add', addRestaurant);
ownerRouter.patch('/edit/:id', editRestaurantById);
ownerRouter.delete('/delete/:id', deleteRestaurantById);
ownerRouter.get('/allRestaurantOfOwner', getAllRestaurantOfOwner);
ownerRouter.get('/allInactiveRestaurantOfOwner', getAllInactiveRestaurantOfOwner);

module.exports = ownerRouter;