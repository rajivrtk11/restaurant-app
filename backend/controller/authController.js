const { SECRET_KEY } = require("../config/secrets");
const userModel = require("../Model/usermodel");
const jwt = require('jsonwebtoken');

async function signup(req, res) {
    try {
        let user = req.body;
        console.log(user)
        let newUser = await userModel.create({
            name: user.name,
            email: user.email,
            password: user.password,
            confirmPassword: user.confirmPassword,
            role: user.role
            // restId:["1","2"]
        });
        // console.log(newUser);
        res.status(201).json({
            message: "Succesfully Signed up !!",
            data: newUser,
        });
    } catch (error) {
        res.status(501).json({
            message: "Failed to sign up !!",
            error,
        });
    }
}

async function login(req, res) {
    try {
        let { email, password } = req.body;
        let loggedInUser = await userModel.find({ email: email });
        // console.log(loggedInUser)

        if (loggedInUser.length) {
            let user = loggedInUser[0];
            if (user.password == password) {

                // token generation
                let token = jwt.sign({ email: email }, SECRET_KEY, { expiresIn: 60 * 60 });
                // let decoded = jwt.verify(token, SECRET_KEY);
                // console.log(decoded.email) 

                res.status(200).json({
                    "message": "successfully loggedin !!!!",
                    "data": loggedInUser,
                    "token": token
                })
            }
            else {
                res.status(400).json({
                    "message": "email and password do not match"
                })
            }
        }
        else {
            res.status(400).json({
                "message": "No user found"
            })
        }
    }
    catch (err) {
        res.json({
            "message": "failed to login",
            "error": err
        })
    }
}

async function isLoggedIn(req, res, next) {
    try {
        let { token } = req.body;
        let decoded = jwt.verify(token, SECRET_KEY);
        // console.log(decoded.email) 
        // console.log(token);
        if (decoded) {
            next();
        }
        else {
            res.json({
                "message": "login first"
            })
        }
    }
    catch (err) {
        res.json({
            "message": "login first"
        })
    }
}

async function isAuthentic(req, res, next) {
    try {
        let { id } = req.body;
        let user = await userModel.findById(id);
        if (user.role == 'admin') {
            next();
        }
        else {
            res.json({
                "message": "you are not authentic person"
            })
        }
    }
    catch (err) {
        res.json({
            "message": "error occured while authentication"
        })
    }
}

async function ownerAuthentication(req, res, next){
    try {
        let { uid } = req.body;
        let user = await userModel.findById(uid);
        if (user.role == 'admin' || user.role == 'owner' ) {
            next();
        }
        else {
            res.json({
                "message": "you are not authentic person"
            })
        }
    }
    catch (err) {
        res.json({
            "message": "error occured while authentication"
        })
    }
}
module.exports = {
    signup: signup,
    login: login,
    isLoggedIn: isLoggedIn,
    isAuthentic: isAuthentic,
    ownerAuthentication:ownerAuthentication
}