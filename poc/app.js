const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');

app.use(express.json());

app.get("/", (req, res) => {
    res.send("hello word")
})
app.get('/token', async (req, res) => {
    let token = jwt.sign({ foo: 'rajivrtk11@gmail.com' }, 'shhhhh');
    console.log(token);
    let decoded = jwt.verify(token, 'shhhhh');
    console.log(decoded.foo)
    res.send(token)
})

app.get('/payment', protect, doAccess)

async function protect(req, res, next){
    let payload;
    try{
        let token = req.body.token;
         payload = jwt.verify(token, 'shhhhh');
         req.id = payload.foo;
        console.log(payload);
        if(payload){
            next();
        }
        else{
            res.json({
                "message":"login first"
            })
        }
    }
    catch(err){
        res.json({
            "message":"login first",
            payload
        })
    }
}

async function doAccess(req, res){
    res.json({
        "message":`${req.id} your payment done`
    })
}
app.listen(4000, () => {
    console.log("app is listening at http://localhost:4000");
})

